#include <iostream>
#include <cstdio>

using namespace std;

int main(int argc, char** argv)
{
	int ki_table[48];
	FILE *ftable = fopen("ki_table.txt", "r");
	for (int i = 0; i < 48; i++)
		fscanf(ftable, "%d", &ki_table[i]);
	fclose(ftable);

	FILE *ftablesi = fopen("si_table.txt", "r");
	int si_table[512];
	for (int i = 0; i < 512; i++)
		fscanf(ftablesi, "%d", &si_table[i]);
	fclose(ftablesi);

	FILE *fip1 = fopen("ip1.txt", "r");
	int ip1[64];
	for (int i = 0; i < 64; i++)
		fscanf(fip1, "%d", &ip1[i]);
	fclose(fip1);

	FILE *fp = fopen("perestanovka_p.txt", "r");
	int perestanovka_p[32];
	for (int i = 0; i < 32; i++)
		fscanf(fp, "%d", &perestanovka_p[i]);
	fclose(fp);

	FILE *fkey = fopen(argv[2], "rb");
	char kkk[8];
	fread(kkk, 1, 8, fkey);
	fclose(fkey);

	unsigned char key[64];
	//kkk -> key
	for (int i = 0; i < 8; i++)
	{
		unsigned char temp = 0x80;
		for (int j = 0; j < 8; j++)
		{
			if ((kkk[i] & temp) == 0)
				key[8 * i + j] = 0;
			else
				key[8 * i + j] = 1;
			temp = temp >> 1;
		}
	}
	//kkk -> key (end)

	//������ c0 � d0
	unsigned char c0[28], d0[28];
	int count = 0;
	for (int j = 56; j <= 58; j++)
		for (int i = j; i >= 0; i = i - 8)
		{
			c0[count] = key[i];
			count++;
		}
	for (int i = 59; i >= 35; i = i - 8)
	{
		c0[count] = key[i];
		count++;
	}
	count = 0;
	for (int j = 62; j >= 60; j--)
		for (int i = j; i >= 0; i = i - 8)
		{
			d0[count] = key[i];
			count++;
		}
	for (int i = 27; i >= 3; i = i - 8)
	{
		d0[count] = key[i];
		count++;
	}
	//������ c0 � d0 (�����)

	//����� ����� c0 � d0
	unsigned char c0start[28], d0start[28];
	for (int i = 0; i < 28; i++)
	{
		c0start[i] = c0[i];
		d0start[i] = d0[i];
	}
	//����� ����� c0 � d0 (�����)

	FILE *f = fopen(argv[1], "rb");
	fseek(f, 0, SEEK_END);
	int file_len = ftell(f);
	int file_lenth = file_len / 8;
	if (file_len % 8 == 0)
		file_lenth--;
	rewind(f);

	FILE *f_out = fopen(argv[3], "w");
	for (int file_lent = 0; file_lent <= file_lenth; file_lent++)
	{
		for (int i = 0; i < 28; i++)
		{
			c0[i] = c0start[i];
			d0[i] = d0start[i];
		}
		unsigned char aaa[8];
		if (file_len - file_lent * 8 < 8)
		{
			fread(aaa, 1, file_len - file_lent * 8, f);
			for (int i = file_len - file_lent * 8; i < 8; i++)
				aaa[i] = 0;
		}
		else
			fread(aaa, 1, 8, f);

		unsigned char bbb[64];
		//aaa -> bbb
		for (int i = 0; i < 8; i++)
		{
			unsigned char temp = 0x80;
			for (int j = 0; j < 8; j++)
			{
				if ((aaa[i] & temp) == 0)
					bbb[8 * i + j] = 0;
				else
					bbb[8 * i + j] = 1;
				temp = temp >> 1;
			}
		}
		//aaa -> bbb (end)

		//��������� ������������
		unsigned char bbbp[64];
		count = 0;
		for (int j = 57; j <= 63; j = j + 2)
			for (int i = j; i >= 0; i = i - 8)
			{
				bbbp[count] = bbb[i];
				count++;
			}
		for (int j = 56; j <= 62; j = j + 2)
			for (int i = j; i >= 0; i = i - 8)
			{
				bbbp[count] = bbb[i];
				count++;
			}
		//��������� ������������ (�����)

		//������ l0 � r0
		unsigned char l0[32], r0[32];
		for (int i = 0; i < 32; i++)
		{
			r0[i] = bbbp[i];
			l0[i] = bbbp[i + 32];
		}
		//������ l0 � r0 (�����)

		//����� ����������
		for (int q = 1; q <= 16; q++)
		{
			unsigned char cq[28], dq[28];
			//������ �q � dq
			if (q == 1)
			{
				for (int i = 0; i < 28; i++)
				{
					cq[i] = c0[i];
					dq[i] = d0[i];
				}
			}
			else
			{
				for (int i = 27; i >= 0; i--)
				{
					if (i != 0)
						cq[i] = c0[i - 1];
					else
						cq[i] = c0[27];
				}
				for (int i = 0; i < 28; i++)
					c0[i] = cq[i];
				for (int i = 27; i >= 0; i--)
				{
					if (i != 0)
						dq[i] = d0[i - 1];
					else
						dq[i] = d0[27];
				}
				for (int i = 0; i < 28; i++)
					d0[i] = dq[i];
				if ((q != 2) && (q != 9) && (q != 16))
				{
					for (int i = 27; i >= 0; i--)
					{
						if (i != 0)
							cq[i] = c0[i - 1];
						else
							cq[i] = c0[27];
					}
					for (int i = 0; i < 28; i++)
						c0[i] = cq[i];
					for (int i = 27; i >= 0; i--)
					{
						if (i != 0)
							dq[i] = d0[i - 1];
						else
							dq[i] = d0[27];
					}
					for (int i = 0; i < 28; i++)
						d0[i] = dq[i];
				}
			}
			//������ �q � dq (�����)
			unsigned char kq[48];
			unsigned char cqdq[56];
			for (int i = 0; i < 28; i++)
			{
				cqdq[i] = cq[i];
				cqdq[i + 28] = dq[i];
			}

			//������ kq
			for (int i = 0; i < 48; i++)
				kq[i] = cqdq[ki_table[i] - 1];
			//������ kq (�����)
			
			//������ E(l i - 1)
			unsigned char elq[48];
			elq[0] = l0[31];
			for (int i = 1; i < 6; i++)
			{
				elq[i] = l0[i - 1];
			}
			for (int j = 1; j <= 6; j++)
			{
				for (int i = 6 * j; i < 6 * (j + 1); i++)
				{
					elq[i] = l0[4 * j + (i - 6 * j) - 1];
				}
			}
			for (int i = 42; i <= 46; i++)
			{
				elq[i] = l0[i - 15];
			}
			elq[47] = l0[0];
			//������ E(l i - 1) (�����)

			//elq + kq
			for (int i = 0; i < 48; i++)
			{
				if ((elq[i] + kq[i]) == 2)
					elq[i] = 0;
				else
					elq[i] = elq[i] + kq[i];
			}
			//elq + kq (end)

			//������ si... � lq!
			unsigned char lqpre[32];
			for (int k = 0; k < 8; k++)
			{
				int str = 0;
				if (elq[k * 6] == 1)
					str += 2;
				if (elq[k * 6 + 5] == 1)
					str += 1;
				int col = 0;
				if (elq[k * 6 + 1] == 1)
					col += 8;
				if (elq[k * 6 + 2] == 1)
					col += 4;
				if (elq[k * 6 + 3] == 1)
					col += 2;
				if (elq[k * 6 + 4] == 1)
					col += 1;
				int si_cur = si_table[k * 64 + str * 16 + col];
				unsigned char v_lq[4];
				if (si_cur / 8 == 1)
					v_lq[0] = 1;
				else
					v_lq[0] = 0;
				si_cur = si_cur % 8;
				if (si_cur / 4 == 1)
					v_lq[1] = 1;
				else
					v_lq[1] = 0;
				si_cur = si_cur % 4;
				if (si_cur / 2 == 1)
					v_lq[2] = 1;
				else
					v_lq[2] = 0;
				si_cur = si_cur % 2;
				if (si_cur == 1)
					v_lq[3] = 1;
				else
					v_lq[3] = 0;
				for (int i = 0; i < 4; i++)
					lqpre[k * 4 + i] = v_lq[i];
			}
			//������ si... � rq! (�����)

			//������������ P
			unsigned char lq[32];
			for (int i = 0; i < 32; i++)
			{
				lq[i] = lqpre[perestanovka_p[i] - 1];
			}
			//������������ P (�����)

			//������� �������� lq � rq
			unsigned char lq_final[32];
			for (int i = 0; i < 32; i++)
			{
				if (r0[i] + lq[i] == 2)
					lq_final[i] = 0;
				else
					lq_final[i] = r0[i] + lq[i];
			}
			for (int i = 0; i < 32; i++)
			{
				r0[i] = l0[i];
				l0[i] = lq_final[i];
			}
			//������� �������� lq � rq (�����)
		}
		//����� ���������� (�����)

		//�������� ������������
		unsigned char l0r0[64];
		for (int i = 0; i < 32; i++)
		{
			l0r0[i] = l0[i];
			l0r0[i + 32] = r0[i];
		}
		unsigned char output[64];
		for (int i = 0; i < 64; i++)
		{
			output[i] = l0r0[ip1[i] - 1];
		}
		//�������� ������������ (�����)

		//������� ������ � ascii
		unsigned char out[8];
		for (int i = 0; i < 8; i++)
			out[i] = 0;
		for (int i = 0; i < 8; i++)
		{
			unsigned char temp = 0x80;
			for (int j = 0; j < 8; j++)
			{
				if (output[i * 8 + j] == 1)
					out[i] = (out[i] | temp);
				temp = temp >> 1;
			}
		}
		//������� ������ � ascii (�����)

		for (int i = 0; i < 8; i++)
			fprintf(f_out, "%c", out[i]);

	}

	fclose(f);
	fclose(f_out);
	return 0;
}